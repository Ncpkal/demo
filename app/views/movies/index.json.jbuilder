json.array!(@movies) do |movie|
  json.extract! movie, :id, :name, :person_id, :person_type
  json.url movie_url(movie, format: :json)
end
