class Actor < ActiveRecord::Base
	has_many :movies, as: :person
end
