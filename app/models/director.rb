class Director < ActiveRecord::Base
	has_one :movie, as: :person
end
