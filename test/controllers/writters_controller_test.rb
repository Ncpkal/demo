require 'test_helper'

class WrittersControllerTest < ActionController::TestCase
  setup do
    @writter = writters(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:writters)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create writter" do
    assert_difference('Writter.count') do
      post :create, writter: { name: @writter.name }
    end

    assert_redirected_to writter_path(assigns(:writter))
  end

  test "should show writter" do
    get :show, id: @writter
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @writter
    assert_response :success
  end

  test "should update writter" do
    patch :update, id: @writter, writter: { name: @writter.name }
    assert_redirected_to writter_path(assigns(:writter))
  end

  test "should destroy writter" do
    assert_difference('Writter.count', -1) do
      delete :destroy, id: @writter
    end

    assert_redirected_to writters_path
  end
end
